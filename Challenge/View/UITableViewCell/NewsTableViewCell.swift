//
//  NewsTableViewCell.swift
//  Challenge
//
//  Created by Vagner Orlandi on 02/02/2019.
//  Copyright © 2019 Vagner Orlandi. All rights reserved.
//

import UIKit

class NewsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var tituloLabel: UILabel!
    
    @IBOutlet weak var dataLabel: UILabel!
    
    @IBOutlet weak var fotoImageView: UIImageView!
    
    @IBOutlet weak var resumoLabel: UILabel!
    
    @IBOutlet weak var categoriasLabel: UILabel!
    
    @IBOutlet weak var favoritoButton: UIButton!
    
    var news:News? = nil {
        didSet {
            self.configureView()
        }
    }
    
    /*
     Permitir para que a ação possa ser escutada
     */
    var onFavoriteButtonAction:((News?) -> Void)? = nil
    
    // MARK: - UIView
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.favoritoButton.setBackgroundImage(#imageLiteral(resourceName: "favorite_active"), for: UIControl.State.selected)
    }
    
    // MARK: - NewsTableViewCell
    
    func configureView() {
        guard let _news = self.news,
            let _ = self.tituloLabel
        else {
            return
        }
        self.tituloLabel.text = _news.titulo
        self.dataLabel.text = _news.data?.string(format: "dd/MM/yyyy")
        
        if _news.fotoPrincipal == nil {
            _news.fotoPrincipalURL?.downloadImage(completion: { [weak self] (foto) in
                _news.fotoPrincipal = foto
                //Apenas atualiza a imagem da celula, se ainda estiver exibindo a mesma noticia
                if let id = self?.news?.id, id == _news.id {
                    self?.fotoImageView.image = foto
                }
            })
        } else {
            self.fotoImageView.image = _news.fotoPrincipal
        }
        
        self.resumoLabel.text = _news.conteudo?.stripOutHtml()
        self.favoritoButton.isSelected = _news.isFavorite
        if let tags = _news.categoriaNomes, !tags.isEmpty {
            self.categoriasLabel.text = "Tags: \(tags.joined(separator: ", "))"
        } else {
            self.categoriasLabel.text = ""
        }
        
        self.favoritoButton.isSelected = _news.isFavorite
    }

    // MARK: - IBActions
    @IBAction func favoritoButtonAction(_ sender: UIButton) {
        self.onFavoriteButtonAction?(news)
    }
}
