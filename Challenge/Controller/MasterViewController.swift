//
//  MasterViewController.swift
//  Challenge
//
//  Created by Vagner Orlandi on 01/02/2019.
//  Copyright © 2019 Vagner Orlandi. All rights reserved.
//

import UIKit

class MasterViewController: UITableViewController {
    
    var newsViewModel:NewsViewModel? = NewsViewModel()

    var detailViewController: DetailViewController? = nil
    
    var isLoadingFromServer:Bool = false {
        didSet {
            UIView.animate(withDuration: 0.25, animations: {
                self.loadingView.alpha = self.isLoadingFromServer ? 1 : 0
            }) { (_) in
                if self.isLoadingFromServer {
                    (self.loadingView.viewWithTag(1) as? UIActivityIndicatorView)?.startAnimating()
                } else {
                    (self.loadingView.viewWithTag(1) as? UIActivityIndicatorView)?.stopAnimating()
                }
            }
        }
    }
    
    lazy var loadingView:UIView = { [unowned self] in
        let view = UIView()
        view.alpha = 0
        view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        view.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(view)
        NSLayoutConstraint.activate([
            view.leftAnchor.constraint(equalTo: self.tableView.leftAnchor),
            view.topAnchor.constraint(equalTo: self.tableView.topAnchor),
            view.widthAnchor.constraint(equalTo: self.tableView.widthAnchor),
            view.heightAnchor.constraint(equalTo: self.tableView.heightAnchor),
        ])
        
        let activityIndicator = UIActivityIndicatorView.init(style: UIActivityIndicatorView.Style.whiteLarge)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(activityIndicator)
        activityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        activityIndicator.tag = 1
        activityIndicator.hidesWhenStopped = true
        return view
    }()
    
    lazy var alertView: UIAlertController = {
        let alert = UIAlertController(title: "Ops!", message: "", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil))
        return alert
    }()

    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let organizeButton = UIBarButtonItem(image: #imageLiteral(resourceName: "sort"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(orderList(_:)))
        navigationItem.rightBarButtonItem = organizeButton
        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
        
        self.tableView.register(UINib(nibName: "NewsTableViewCell", bundle: nil), forCellReuseIdentifier: "NewsCell")
        self.tableView.tableFooterView = UIView()
        self.tableView.refreshControl = UIRefreshControl()
        self.tableView.refreshControl?.addTarget(self, action: #selector(updateNewsFromServer(_:)), for: .valueChanged)
        
        self.newsViewModel?.newsListDidChange = { [weak self] () in
            self?.tableView.reloadData()
        }
        
        self.newsViewModel?.loadSavedNews()
        
        self.isLoadingFromServer = true
        self.newsViewModel?.reloadFromServer() { [weak self] (error) in
            self?.isLoadingFromServer = false
            if error != nil {
                guard let strongSelf = self else { return }
                strongSelf.alertView.message = error?.userMessage
                strongSelf.present(strongSelf.alertView, animated: true, completion: nil)
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
            controller.newsViewModel = self.newsViewModel
            controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
            controller.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: controller.favoriteButton)
            controller.navigationItem.leftItemsSupplementBackButton = true
        }
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.newsViewModel?.newsList?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell
        
        if let newsCell = tableView.dequeueReusableCell(withIdentifier: "NewsCell", for: indexPath) as? NewsTableViewCell {
            if let news = self.newsViewModel?.newsList?[indexPath.row] {
                newsCell.news = news
                if newsCell.onFavoriteButtonAction == nil {
                    newsCell.onFavoriteButtonAction = { [weak self] (cellNews) in
                        self?.newsViewModel?.toogleFavorite(for: cellNews)
                    }
                }
            }
            cell = newsCell
        } else {
            cell = UITableViewCell()
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "showDetail", sender: nil)
        
        if let indexPath = tableView.indexPathForSelectedRow {
            self.newsViewModel?.selectedNews = self.newsViewModel?.newsList?[indexPath.row]
        }
    }
    
    // MARK: - Actions
    
    @objc
    func orderList(_ sender: Any) {
        let sortMode = self.newsViewModel?.currentDateFieldSorting == .ascending ? NewsViewModel.SortMode.descending : .ascending
        self.newsViewModel?.orderByDate(sortMode)
        self.tableView.reloadSections(IndexSet(integer: 0), with: UITableView.RowAnimation.fade)
    }
    
    @objc
    func updateNewsFromServer(_ sender: Any) {
        self.tableView.refreshControl?.attributedTitle = NSAttributedString(string: "Carregando noticias")
        self.newsViewModel?.reloadFromServer { [weak self] (error) in
            self?.refreshControl?.endRefreshing()
            self?.refreshControl?.attributedTitle = nil
            if error != nil {
                //Apenas para garantir que a tableview finalizou a animação do `refreshControl`
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    guard let strongSelf = self else { return }
                    strongSelf.alertView.message = error?.userMessage
                    strongSelf.present(strongSelf.alertView, animated: true, completion: nil)
                })
            }
        }
    }
}
