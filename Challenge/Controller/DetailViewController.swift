//
//  DetailViewController.swift
//  Challenge
//
//  Created by Vagner Orlandi on 01/02/2019.
//  Copyright © 2019 Vagner Orlandi. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var fotoImageView: UIImageView!
    
    @IBOutlet weak var tituloLabel: UILabel!
    
    @IBOutlet weak var noticiaInfoLabel: UILabel!
    
    @IBOutlet weak var categoriaLabel: UILabel!
    
    @IBOutlet weak var conteudoLabel: UILabel!
    
    lazy var favoriteButton:UIButton = { [unowned self] in
        let btn = UIButton()
        btn.addTarget(self, action: #selector(makeFavorite(_:)), for: UIControl.Event.touchUpInside)
        btn.setImage(#imageLiteral(resourceName: "favorite_inactive"), for: UIControl.State.normal)
        btn.setImage(#imageLiteral(resourceName: "favorite_active"), for: UIControl.State.selected)
        let imageSize = #imageLiteral(resourceName: "favorite_inactive").size
        btn.widthAnchor.constraint(equalToConstant: imageSize.width).isActive = true
        btn.heightAnchor.constraint(equalToConstant: imageSize.height).isActive = true
        return btn
    }()
    
    var isLoadingFromServer:Bool = false {
        didSet {
            UIView.animate(withDuration: 0.25, animations: {
                self.loadingView.alpha = self.isLoadingFromServer ? 1 : 0
            }) { (_) in
                if self.isLoadingFromServer {
                    (self.loadingView.viewWithTag(1) as? UIActivityIndicatorView)?.startAnimating()
                } else {
                    (self.loadingView.viewWithTag(1) as? UIActivityIndicatorView)?.stopAnimating()
                }
            }
        }
    }
    
    lazy var loadingView:UIView = { [unowned self] in
        let view = UIView()
        view.alpha = 0
        view.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(view)
        NSLayoutConstraint.activate([
            view.leftAnchor.constraint(equalTo: self.view.leftAnchor),
            view.topAnchor.constraint(equalTo: self.conteudoLabel.topAnchor),
            view.widthAnchor.constraint(equalTo: self.view.widthAnchor),
            view.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
            ])
        
        let activityIndicator = UIActivityIndicatorView.init(style: UIActivityIndicatorView.Style.gray)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(activityIndicator)
        activityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        activityIndicator.tag = 1
        activityIndicator.hidesWhenStopped = true
        return view
        }()
    
    var newsViewModel:NewsViewModel? = nil {
        didSet {
            //Registra para modificar a view, quando a noticia for alterada
            self.newsViewModel?.selectedNewsDidChange = { [weak self] in
                self?.loadNewsIfNeeded()
            }
        }
    }
    
    lazy var alertView: UIAlertController = {
        let alert = UIAlertController.init(title: "Ops!", message: "", preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "OK", style: UIAlertAction.Style.cancel, handler: nil))
        return alert
    }()

    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadNewsIfNeeded()
    }
    
    // MARK: - Actions
    
    @objc
    func makeFavorite(_ sender: Any) {
        guard let news = self.newsViewModel?.selectedNews else { return }
        self.newsViewModel?.toogleFavorite(for: news)
    }
    
    // MARK: - DetailViewController
    
    func loadNewsIfNeeded() {
        guard let news = self.newsViewModel?.selectedNews else { return }
        if !news.isLoaded {
            self.isLoadingFromServer = true
            self.newsViewModel?.loadMore(news) { [weak self] (_, error) in
                self?.isLoadingFromServer = false
                if error != nil {
                    guard let strongSelf = self else { return }
                    strongSelf.alertView.message = error?.userMessage
                    strongSelf.present(strongSelf.alertView, animated: true, completion: nil)
                }
            }
            self.configureView()
        } else {
            self.configureView()
        }
    }
    
    /*
     Configura as views conforme dados em `NewsViewModel.selectedNews`.
     
     */
    func configureView() {
        guard
            let _ = self.categoriaLabel, //Prevenir de acessar as views antes de serem instanciadas
            let news = self.newsViewModel?.selectedNews //dados para preencher a view
            else { return }
        
        self.favoriteButton.isSelected = news.isFavorite
        
        if news.fotoPrincipal == nil {
            news.fotoPrincipalURL?.downloadImage(completion: { [weak self] (foto) in
                news.fotoPrincipal = foto
                self?.fotoImageView.image = foto
            })
        } else {
            self.fotoImageView.image = news.fotoPrincipal
        }
        
        self.tituloLabel.text = news.titulo
        var info:String = ""
        if let autor = news.autor, !autor.isEmpty {
            info += "Por: \(autor)"
        }
        
        if let data = news.data, let stringData = data.string(format: "dd/MM/yyyy hh:mm") {
            info += (!info.isEmpty ? " em " : "")
            info += stringData
        }
        
        self.noticiaInfoLabel.text = info
        if let categorias = news.categoriaNomes, !categorias.isEmpty {
            self.categoriaLabel.text = "Tags: \(categorias.joined(separator: ", "))"
        } else {
            self.categoriaLabel.text = ""
        }
        
        //Exibir conteudo da noticia caso já carregada por completo
        if news.isLoaded {
            self.conteudoLabel.text = news.conteudo?.stripOutHtml()
        } else {
            self.conteudoLabel.text = ""
        }
    }
}
