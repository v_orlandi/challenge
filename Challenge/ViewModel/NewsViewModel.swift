//
//  NewsViewModel.swift
//  Challenge
//
//  Created by Vagner Orlandi on 01/02/2019.
//  Copyright © 2019 Vagner Orlandi. All rights reserved.
//

import Foundation
import UIKit

class NewsViewModel {
    /*
     Chaves que a classe utiliza ao armazenar em UserDefaults
     */
    struct UserDefaultsKeys {
        static let newsList:String = "SwiftClassNews"
        static let imageName:String = "SwiftClassNewsImage"
    }
    
    enum SortMode {
        case ascending
        case descending
        case none
    }
    
    internal var newsList:[News]? {
        willSet {
            guard let newsList = self.newsList,
                let modifiedNewsList = newValue
            else {
                self.newsListWillChange?(newValue)
                return
            }
            
            //Verifica novos ponteiros no Array para idenficicar as novas instancias
            var newElements:[News] = []
            let pointersOld:[UnsafeMutableRawPointer] = newsList.map({Unmanaged.passUnretained($0).toOpaque()})
            let pointersNew:[UnsafeMutableRawPointer] = modifiedNewsList.map({Unmanaged.passUnretained($0).toOpaque()})
            let oldSet = Set<UnsafeMutableRawPointer>(pointersOld)
            let newSet = Set<UnsafeMutableRawPointer>(pointersNew)
            let pointersNewElements = newSet.subtracting(oldSet)
            
            //Preenche o array com novos objetos
            for i in pointersNewElements {
                if let index = pointersNew.firstIndex(where: {$0 == i}) {
                    newElements.append(modifiedNewsList[index])
                    if modifiedNewsList[index].id == self.selectedNews?.id {
                        self.selectedNews = modifiedNewsList[index]
                    }
                }
            }
            
            self.newsListWillChange?(newElements)
        }
        
        didSet {
            self.newsListDidChange?()
        }
    }
    
    var newsListWillChange:(([News]?) -> Void)? = nil
    
    var newsListDidChange:(() -> Void)? = nil
    
    var currentDateFieldSorting:SortMode = .none
    
    private var newsAPI:NewsAPI
    
    var selectedNews:News? = nil {
        didSet {
            self.selectedNewsDidChange?()
        }
    }
    
    var selectedNewsDidChange:(() -> Void)? = nil
    
    //MARK: - init
    
    init(news:[News]? = nil, host:String? = nil) {
        self.newsList = news
        let apiHeaders:[String : String] = [:]
        if let altHost = host {
            self.newsAPI = NewsAPI(host: altHost, customHeaders: apiHeaders)
        } else {
            self.newsAPI = NewsAPI(host: APIHosts.default, customHeaders: apiHeaders)
        }
    }
    
    // MARK: Public methods
    
    /*
     Carrega as noticias que foram armazenadas em UserDefaults
     */
    func loadSavedNews() {
        if let savedNews = NewsViewModel.getAllNews() {
            //Adiciona apenas noticias unicas
            self.newsList = Array(Set(self.newsList ?? []).union(savedNews))
        }
    }
    
    /*
     Requisita a lista de noticias no servidor
     */
    func reloadFromServer(completion: @escaping (WebserviceError?) -> Void) {
        self.newsAPI.getList { [weak self] (news, error) in
            if error == nil {
                //Adiciona apenas noticias unicas
                self?.newsList = Array(Set(self?.newsList ?? []).union(news ?? []))
            }
            completion(error)
        }
    }
    
    /*
     Requisita a noticia completa no servidor
     */
    func loadMore(_ news: News, completion: @escaping (News?, WebserviceError?) -> Void) {
        self.newsAPI.getNews(by: news.id) { (fullNews, error) in
            var modifiedNews:News? = nil
            if let index = self.newsList?.firstIndex(where: {$0.id == news.id}), error == nil {
                if var news = self.newsList?[index] {
                    let isFavorite = news.isFavorite
                    let image = news.fotoPrincipal
                    news.merge(with: fullNews)
                    news.isLoaded = true
                    news.fotoPrincipal = image
                    news.isFavorite = isFavorite
                    self.newsList?[index] = news
                    modifiedNews = news
                }
            }
            
            completion(modifiedNews, error)
        }
    }
    
    /**
     Modifica o status do propriedade `isFavorito`.
     
     Notícias favoritadas são armazenadas no dispositivo para leitura off-line
     */
    func toogleFavorite(for news:News?) {
        guard let news = news else { return }
        news.isFavorite = !news.isFavorite
        if self.newsList?.contains(news) ?? false {
            if news.isFavorite {
                NewsViewModel.saveNews(news)
            } else {
                NewsViewModel.removeNews(news)
            }
            self.newsListDidChange?()
            if self.selectedNews?.id == news.id {
                self.selectedNewsDidChange?()
            }
        }
    }
    
    /*
     Ordena a lista de noticia usando a propriedade `data`
     */
    func orderByDate(_ sortMode: SortMode) {
        self.currentDateFieldSorting = sortMode
        switch sortMode {
        case .ascending:
            self.newsList?.sort(by: {($0.data ?? .distantPast) < ($1.data ?? .distantPast)})
        case .descending:
            self.newsList?.sort(by: {($0.data ?? .distantPast) > ($1.data ?? .distantPast)})
        case .none:
            break
        }
    }
    
    // MARK: - UserDefaults
    
    /*
     Salva a notícia no dispositivo
     */
    private static func saveNews(_ news: News) {
        var newsArray:[News] = NewsViewModel.getAllNews() ?? []
        newsArray.append(news)
        newsArray = Array(Set(newsArray))
        
        do {
            let newsArrayJsonData = try JSONEncoder().encode(newsArray)
            UserDefaults.standard.set(newsArrayJsonData, forKey: UserDefaultsKeys.newsList)
        } catch {
            debugPrint("NewsViewModel::ERROR::\(error.localizedDescription) - News.id:\(news.id)")
        }
        
        let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        
        if let fileURL = documentDirectory?.appendingPathComponent("\(UserDefaultsKeys.imageName)-\(news.id).png") {
            do {
                try news.fotoPrincipal?.pngData()?.write(to: fileURL)
            } catch {
                debugPrint("NewsViewModel::ERROR::\(error.localizedDescription) - News.id:\(news.id)")
            }
        }
    }
    
    /*
     Remove a notícia no dispositivo
     */
    private static func removeNews(_ news: News) {
        var newsArray:[News] = NewsViewModel.getAllNews() ?? []
        newsArray.removeAll(where: {$0.id == news.id})
        
        do {
            let newsArrayJsonData = try JSONEncoder().encode(newsArray)
            UserDefaults.standard.set(newsArrayJsonData, forKey: UserDefaultsKeys.newsList)
        } catch {
            debugPrint("NewsViewModel::ERROR::\(error.localizedDescription) - News.id:\(news.id)")
        }
        
        let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        if let fileURL = documentDirectory?.appendingPathComponent("\(UserDefaultsKeys.imageName)-\(news.id).png") {
            do {
                try FileManager.default.removeItem(at: fileURL)
            } catch {
                debugPrint("NewsViewModel::ERROR::\(error.localizedDescription) - News.id:\(news.id)")
            }
        }
    }
    
    /*
     Carrega todas as noticias salvas no dispositivo
     */
    private static func getAllNews() -> [News]? {
        if let newsData = UserDefaults.standard.data(forKey: UserDefaultsKeys.newsList) {
            let newsArray = try? JSONDecoder().decode([News].self, from: newsData)
            
            newsArray?.forEach({ (news) in
                news.isFavorite = true
                news.isLoaded = news.status != nil
                
                let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
                if let filePathString = documentDirectory?.absoluteString.appending("\(UserDefaultsKeys.imageName)-\(news.id).png") {
                    news.fotoPrincipal = UIImage(contentsOfFile: filePathString)
                }
            })
            return newsArray
        } else {
            return nil
        }
    }
}
