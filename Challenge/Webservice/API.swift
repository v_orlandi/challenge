//
//  API.swift
//  Challenge
//
//  Created by Vagner Orlandi on 01/02/2019.
//  Copyright © 2019 Vagner Orlandi. All rights reserved.
//

import Foundation

protocol API {
    var webservice:Webservice { get }
    
    var path:String { get }
    
    init(host: String, customHeaders:[String : String])
}

extension API {
    func mergeHeaders(lhs:[String : String], rhs:[String : String]?) -> [String : String] {
        guard let newHeaders = rhs else {
            return lhs
        }
        return lhs.merging(newHeaders) { (_, new) in new }
    }
}

struct APIHosts {
    static let `default` = "http://mobilesaude.com.br"
}
