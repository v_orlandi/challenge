//
//  Webservice.swift
//  Challenge
//
//  Created by Vagner Orlandi on 01/02/2019.
//  Copyright © 2019 Vagner Orlandi. All rights reserved.
//

import Foundation

struct WebserviceError:Error {
    let debugMessage:String
    let userMessage:String
    
    init(debugMessage:String, userMessage:String) {
        self.debugMessage = debugMessage
        self.userMessage = userMessage
    }
    
    //Mensagens para UI
    static let notFoundMessage     = "O recurso não pode ser encontrado no momento. Tente mais tarde."
    static let notAvailableMessage = "O recurso não pode ser encontrado no momento. Tente mais tarde."
}

struct Webservice {
    typealias Result = (Data?, WebserviceError?) -> Void
    
    struct HttpMethod {
        static let get:String = "GET"
        static let post:String = "POST"
    }
    
    struct RequestConfig {
        static let defaultTimeout:Double = 60
    }
    
    let host:String
    
    let path:String
    
    let session = URLSession(configuration: .default)
    
    var url:URL? {
        get {
            return URL(string: "\(self.host)/\(self.path)")
        }
    }
    
    @discardableResult func get(_ resource:String, completion: @escaping Result) -> URLSessionDataTask? {
        guard let resourceUrl = self.url?.appendingPathComponent(resource) else {
            let error = WebserviceError(
                debugMessage: WebserviceError.d(String(describing: self), #function, message: "URL não pode ser instanciada"),
                userMessage: WebserviceError.notFoundMessage
            )
            completion(nil, error)
            return nil
        }
        
        var request = URLRequest(url: resourceUrl, cachePolicy: URLRequest.CachePolicy.reloadRevalidatingCacheData, timeoutInterval: RequestConfig.defaultTimeout)
        request.httpMethod = HttpMethod.get
        
        let dataTask = self.session.dataTask(with: request) { (responseData, responseStatus, responseError) in
            let webserviceError:WebserviceError? = self.validadeResponse(data: responseData, response: responseStatus, error: responseError)
            completion(responseData, webserviceError)
        }
        dataTask.resume()
        return dataTask
    }
    
    private func validadeResponse(data responseData:Data?, response responseStatus:URLResponse?, error responseError:Error?) -> WebserviceError? {
        if let error = responseError {
            //Erro na requisição
            return WebserviceError(
                debugMessage: WebserviceError.d(String(describing: self), #function, message: error.localizedDescription),
                userMessage: WebserviceError.notAvailableMessage
            )
        } else if let response = responseStatus,
            let _ = responseData {
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode < 200 || httpResponse.statusCode >= 300 {
                    return WebserviceError(
                        debugMessage: WebserviceError.d(String(describing: self), #function, message: "A requisição retornou um erro"),
                        userMessage: WebserviceError.notAvailableMessage
                    )
                }
            }
            return nil
        } else {
            return WebserviceError(
                debugMessage: WebserviceError.d(String(describing: self), #function, message: "A requisição retornou um erro"),
                userMessage: WebserviceError.notAvailableMessage
            )
        }
    }
}
