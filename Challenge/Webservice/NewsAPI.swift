//
//  NewsAPI.swift
//  Challenge
//
//  Created by Vagner Orlandi on 01/02/2019.
//  Copyright © 2019 Vagner Orlandi. All rights reserved.
//

import Foundation

class NewsAPI:API {
    var webservice: Webservice
    
    var path:String = "challenge"
    
    struct Resource {
        static var newsList:String = "lista.json"
        static var newsById:String = "{id}.json"
    }
    
    required init(host: String, customHeaders: [String : String]) {
        self.webservice = Webservice(host: host, path: self.path)
    }
    
    func getList(onResult: @escaping ([News]?, WebserviceError?) -> Void) {
        self.webservice.get(Resource.newsList) { (data, error) in
            var result:[News]? = nil
            if let jsonData = data {
                let jsonDecoder = JSONDecoder()
                jsonDecoder.dateDecodingStrategy = .formatted(DateFormatter.webserviceDefault)
                result = try? jsonDecoder.decode(Array<News>.self, from: jsonData)
            }
            
            DispatchQueue.main.async {
                onResult(result, error)
            }
        }
    }
    
    func getNews(by id:Int, onResult: @escaping (News?, WebserviceError?) -> Void) {
        self.webservice.get(Resource.newsById.replacingOccurrences(of: "{id}", with: String(id))) { (data, error) in
            var result:News? = nil
            if let jsonData = data {
                let jsonDecoder = JSONDecoder()
                jsonDecoder.dateDecodingStrategy = .formatted(DateFormatter.webserviceDefault)
                result = try? jsonDecoder.decode(News.self, from: jsonData)
            }
            DispatchQueue.main.async {
                onResult(result, error)
            }
        }
    }
}
