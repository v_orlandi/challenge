//
//  News.swift
//  Challenge
//
//  Created by Vagner Orlandi on 01/02/2019.
//  Copyright © 2019 Vagner Orlandi. All rights reserved.
//

import Foundation
import UIKit

/*
 Classe que representa uma notícia
 
 **Obs:** Nomes em portugues para mapear com as propriedades que retornam do servidor
 */
class News: Codable, MergeableModel, Equatable, Hashable {
    var hashValue: Int {
        //Implementação básica
        return self.id.hashValue
    }
    
    static func == (lhs: News, rhs: News) -> Bool {
        return lhs.id == rhs.id
    }
    
    //News
    let id:Int
    var titulo:String?           = nil
    var conteudo:String?         = nil
    var data:Date?               = nil
    var dataValidade:Date?       = nil
    var idioma:String?           = nil
    var codigoOperadora:Int?     = nil
    var fotoPrincipalURL:URL?    = nil
    var permalink:String?        = nil
    var postTipoId:Int?          = nil
    var status:String?           = nil
    var widthExibicao:Double?    = nil
    var heightExibicao:Double?   = nil
    var tipoExibicao:String?     = nil
    var exibirTitulo:String?     = nil
    var podcast:URL?             = nil
    var video:URL?               = nil
    var autor:String?            = nil
    var formato:Int?             = nil
    var ambiente:String?         = nil
    var categorias:[Int]?        = nil
    var tags:[String]?           = nil
    var galeriaFotos:String?     = nil
    var galeriaArquivos:String?  = nil
    var sanfona:[String]?        = nil
    var categoriaNomes:[String]? = nil
    
    //Preview
    let sumario:String?          = nil
    let dataPublicacao:Date?     = nil
    
    //Propridades que não retornam do servidor
    var isFavorite:Bool          = false
    var isLoaded:Bool            = false
    var fotoPrincipal:UIImage?   = nil
    
    private enum CodingKeys: String, CodingKey {
        //News
        case id
        case titulo
        case conteudo
        case data
        case dataValidade = "data_validade"
        case idioma
        case codigoOperadora = "cod_operadora"
        case fotoPrincipalURL = "foto_principal_url"
        case permalink
        case postTipoId = "post_tipo_id"
        case status
        case widthExibicao = "width_exibicao"
        case heightExibicao = "height_exibicao"
        case tipoExibicao = "tipo_exibicao"
        case exibirTitulo = "exibir_titulo"
        case podcast
        case video
        case autor
        case formato
        case ambiente
        case categorias
        case tags
        case galeriaFotos = "galeria_fotos"
        case galeriaArquivos = "galeria_arquivos"
        case sanfona
        case categoriaNomes = "categorias_nomes"
        
        //Preview
        case sumario
        case dataPublicacao = "data_publicacao"
    }
}

