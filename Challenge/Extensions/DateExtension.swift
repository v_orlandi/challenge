//
//  DateExtension.swift
//  Challenge
//
//  Created by Vagner Orlandi on 03/02/2019.
//  Copyright © 2019 Vagner Orlandi. All rights reserved.
//

import Foundation

extension Date {
    func string(format:String) -> String? {
        let f = DateFormatter()
        f.dateFormat = format
        return f.string(from: self)
    }
}
