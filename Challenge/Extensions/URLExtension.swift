//
//  UIImageViewExtension.swift
//  Challenge
//
//  Created by Vagner Orlandi on 02/02/2019.
//  Copyright © 2019 Vagner Orlandi. All rights reserved.
//

import Foundation
import UIKit

extension URL {
    func downloadImage(completion: @escaping (UIImage) -> Void) {
        
        URLSession.shared.dataTask(with: self) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                completion(image)
            }
        }.resume()
    }
}
