//
//  ErrorExtensions.swift
//  Challenge
//
//  Created by Vagner Orlandi on 01/02/2019.
//  Copyright © 2019 Vagner Orlandi. All rights reserved.
//

import Foundation

extension Error {
    //TODO: Colocar um parametro para identificar o caso que ocorre o erro
    static func d(_ className:String, _ functionName:String, message:String) -> String {
        return "\(className) :: \(functionName) -> \(message)"
    }
}
