//
//  StringExtensions.swift
//  Challenge
//
//  Created by Vagner Orlandi on 03/02/2019.
//  Copyright © 2019 Vagner Orlandi. All rights reserved.
//

import Foundation

extension String {
    func stripOutHtml() -> String? {
        do {
            guard let data = self.data(using: .unicode) else {
                return nil
            }
            let attributed = try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
            return attributed.string
        } catch {
            return nil
        }
    }
}
