//
//  MergeableProtocol.swift
//  Challenge
//
//  Created by Vagner Orlandi on 02/02/2019.
//  Copyright © 2019 Vagner Orlandi. All rights reserved.
//

import Foundation

protocol MergeableModel { }

extension MergeableModel where Self:Codable {
    mutating func merge(with: Self?) {
        guard let with = with else { return }
        let encoder = JSONEncoder()
        do {
            let selfData = try encoder.encode(self)
            let withData = try encoder.encode(with)

            guard var selfDict = try JSONSerialization.jsonObject(with: selfData) as? [String: Any],
                let withDict = try JSONSerialization.jsonObject(with: withData) as? [String: Any] else {
                    return
            }

            selfDict.merge(withDict) { (_, new) in new }

            let result = try JSONSerialization.data(withJSONObject: selfDict)
            self = try JSONDecoder().decode(Self.self, from: result)
        } catch _ {
            
        }
    }
}
