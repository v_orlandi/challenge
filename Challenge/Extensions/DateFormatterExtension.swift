//
//  DateFormatter.swift
//  Challenge
//
//  Created by Vagner Orlandi on 01/02/2019.
//  Copyright © 2019 Vagner Orlandi. All rights reserved.
//

import Foundation

extension DateFormatter {
    static let webserviceDefault: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.timeZone = TimeZone(secondsFromGMT: -3)
        formatter.locale = Locale(identifier: "pt_BR")
        return formatter
    }()
}
