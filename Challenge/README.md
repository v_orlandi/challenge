#  Mobile Saúde Challenge

## Projeto

- Construir uma tela de listagem e uma tela de detalhe para notícias.

### Tela de Notícias 

- url: `http://mobilesaude.com.br/challenge/lista.json`
- Mostrar em cada célula da notícia o mínimo de título, data e imagem e categoria.
- Criar um botão para mudar a ordenação de data para crescente / decrescente

### Tela de Detalhes 

- url: `http://mobilesaude.com.br/challenge/{id}.json`.
- Mostrar o mínimo de título,imagem,data, conteúdo e categoria da notícia.

### Favoritar
- Em cada notícia, deve ser possível favoritá-la. Uma notícia favorita poderá ser visualizada independente de conexão.

---

## Layout livre

Requisitos mínimos:

- Swift 4.2 
- Versão mínima iOS 10

## Diferenciais 
- Incluir testes unitários

